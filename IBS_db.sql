-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.8-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para bd_ibs
CREATE DATABASE IF NOT EXISTS `bd_ibs` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `bd_ibs`;

-- Volcando estructura para tabla bd_ibs.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `idCategoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigoCategoria` varchar(255) DEFAULT NULL,
  `nombreCategoria` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.categorias: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `idCliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `apellidoCliente` varchar(255) DEFAULT NULL,
  `departamentoCliente` varchar(255) DEFAULT NULL,
  `direccionCliente` varchar(255) DEFAULT NULL,
  `duiCliente` varchar(255) DEFAULT NULL,
  `fechaIngreso` datetime(6) DEFAULT NULL,
  `fechaModificacion` datetime(6) DEFAULT NULL,
  `giro` varchar(255) DEFAULT NULL,
  `nit` varchar(255) DEFAULT NULL,
  `nombreCliente` varchar(255) DEFAULT NULL,
  `nrc` varchar(255) DEFAULT NULL,
  `telefonoCliente` varchar(255) DEFAULT NULL,
  `tipoCliente` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.clientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.compras
CREATE TABLE IF NOT EXISTS `compras` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechaCompra` varchar(255) DEFAULT NULL,
  `fechaIngreso` datetime(6) DEFAULT NULL,
  `numeroFactura` decimal(19,2) DEFAULT NULL,
  `tipoCompra` varchar(255) DEFAULT NULL,
  `totalCompra` varchar(255) DEFAULT NULL,
  `idProveedor` bigint(20) DEFAULT NULL,
  `idUsuario` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8fs9p91fmrvkjhaahjycdojcd` (`idProveedor`),
  KEY `FKo3mtvnga56mb12wnss4uyl1mb` (`idUsuario`),
  CONSTRAINT `FK8fs9p91fmrvkjhaahjycdojcd` FOREIGN KEY (`idProveedor`) REFERENCES `proveedores` (`idProveedor`),
  CONSTRAINT `FKo3mtvnga56mb12wnss4uyl1mb` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.compras: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.comprasproductos
CREATE TABLE IF NOT EXISTS `comprasproductos` (
  `idComprasProductos` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidad` decimal(19,2) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `precioCompra` float NOT NULL,
  `idCompra` bigint(20) DEFAULT NULL,
  `idProducto` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idComprasProductos`),
  KEY `FKt5g01lpkb2oyvxiyiv1wwj7my` (`idCompra`),
  KEY `FKfu265n9su85x9x50uw5bwhflu` (`idProducto`),
  CONSTRAINT `FKfu265n9su85x9x50uw5bwhflu` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`idProducto`),
  CONSTRAINT `FKt5g01lpkb2oyvxiyiv1wwj7my` FOREIGN KEY (`idCompra`) REFERENCES `compras` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.comprasproductos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `comprasproductos` DISABLE KEYS */;
/*!40000 ALTER TABLE `comprasproductos` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.inventarios
CREATE TABLE IF NOT EXISTS `inventarios` (
  `idInventario` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`idInventario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.inventarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `inventarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventarios` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.inventariosproductos
CREATE TABLE IF NOT EXISTS `inventariosproductos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidad` decimal(19,2) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `evento` varchar(255) DEFAULT NULL,
  `idInventario` bigint(20) DEFAULT NULL,
  `idProducto` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb3o8ww0qpy8cgg893d5lwjlv9` (`idInventario`),
  KEY `FKs568jlxx3mjfspdy85veeoyl` (`idProducto`),
  CONSTRAINT `FKb3o8ww0qpy8cgg893d5lwjlv9` FOREIGN KEY (`idInventario`) REFERENCES `inventarios` (`idInventario`),
  CONSTRAINT `FKs568jlxx3mjfspdy85veeoyl` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.inventariosproductos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `inventariosproductos` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventariosproductos` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.marcas
CREATE TABLE IF NOT EXISTS `marcas` (
  `idMarca` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreMarca` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idMarca`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.marcas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.productos
CREATE TABLE IF NOT EXISTS `productos` (
  `idProducto` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigoProducto` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `existencia` int(11) DEFAULT NULL,
  `fechaIngreso` datetime(6) DEFAULT NULL,
  `fechaModificacion` datetime(6) DEFAULT NULL,
  `nombreProducto` varchar(255) DEFAULT NULL,
  `precioCompra` double DEFAULT NULL,
  `precioVenta` double DEFAULT NULL,
  `presentacion` varchar(255) DEFAULT NULL,
  `idCategoria` bigint(20) DEFAULT NULL,
  `idMarca` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idProducto`),
  KEY `FKow1swjfxs59j4g1h53m8iv92x` (`idCategoria`),
  KEY `FKpel0s5lqf5qelvwn3goev78i` (`idMarca`),
  CONSTRAINT `FKow1swjfxs59j4g1h53m8iv92x` FOREIGN KEY (`idCategoria`) REFERENCES `categorias` (`idCategoria`),
  CONSTRAINT `FKpel0s5lqf5qelvwn3goev78i` FOREIGN KEY (`idMarca`) REFERENCES `marcas` (`idMarca`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.productos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.proveedores
CREATE TABLE IF NOT EXISTS `proveedores` (
  `idProveedor` bigint(20) NOT NULL AUTO_INCREMENT,
  `apellidoProveedor` varchar(255) DEFAULT NULL,
  `correoProveedor` varchar(255) DEFAULT NULL,
  `direccionProveedor` varchar(255) DEFAULT NULL,
  `duiProveedor` varchar(255) DEFAULT NULL,
  `nombreProveedor` varchar(255) DEFAULT NULL,
  `telefonoProveedor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idProveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.proveedores: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `idRol` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreRol` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.roles: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`idRol`, `nombreRol`) VALUES
	(1, 'Administrador');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `idUsuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `apellidoUsuario` varchar(255) DEFAULT NULL,
  `correoUsuario` varchar(255) DEFAULT NULL,
  `direccionUsuario` varchar(255) DEFAULT NULL,
  `duiUsuario` varchar(255) DEFAULT NULL,
  `nombreUsuario` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `telefonoUsuario` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `idRol` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  KEY `FK86ctfwhy85rqr9npi3u5eqwo3` (`idRol`),
  CONSTRAINT `FK86ctfwhy85rqr9npi3u5eqwo3` FOREIGN KEY (`idRol`) REFERENCES `roles` (`idRol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`idUsuario`, `apellidoUsuario`, `correoUsuario`, `direccionUsuario`, `duiUsuario`, `nombreUsuario`, `password`, `telefonoUsuario`, `user`, `idRol`) VALUES
	(1, 'Lemus', 'lemus@gmail.com', 'Ahuachapan', '78495689-6', 'Carlos', '$2a$10$V16kEs0QoDxM/UZtTahAoOuiVnvPCmN8NeQWiOI0f6efQCM7uazki', '7849-6523', 'carlos', 1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.ventas
CREATE TABLE IF NOT EXISTS `ventas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado` varchar(255) DEFAULT NULL,
  `fechaVenta` datetime(6) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `numeroFactura` decimal(19,2) DEFAULT NULL,
  `tipoVenta` varchar(255) DEFAULT NULL,
  `totalVenta` float NOT NULL,
  `idCliente` bigint(20) DEFAULT NULL,
  `idUsuario` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcootq144jeoqqiwe76sbnprn9` (`idCliente`),
  KEY `FKprgwlllww3dlgjoenqfosj0w7` (`idUsuario`),
  CONSTRAINT `FKcootq144jeoqqiwe76sbnprn9` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`idCliente`),
  CONSTRAINT `FKprgwlllww3dlgjoenqfosj0w7` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.ventas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;

-- Volcando estructura para tabla bd_ibs.ventasproductos
CREATE TABLE IF NOT EXISTS `ventasproductos` (
  `idVentasproductos` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidad` decimal(19,2) DEFAULT NULL,
  `descuento` float NOT NULL,
  `idProducto` bigint(20) DEFAULT NULL,
  `idVenta` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idVentasproductos`),
  KEY `FKehevhj38t0iy6epq3dqlls7dg` (`idProducto`),
  KEY `FK2ly1uw5f041fqqskcequlaph0` (`idVenta`),
  CONSTRAINT `FK2ly1uw5f041fqqskcequlaph0` FOREIGN KEY (`idVenta`) REFERENCES `ventas` (`id`),
  CONSTRAINT `FKehevhj38t0iy6epq3dqlls7dg` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla bd_ibs.ventasproductos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ventasproductos` DISABLE KEYS */;
/*!40000 ALTER TABLE `ventasproductos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
